﻿using UnityEngine;
using System.Collections;

public class MakeObstacle : MonoBehaviour {

	[SerializeField] float delay;
	[SerializeField] GameObject obstacle;

	// Use this for initialization
	void Start () {
		StartCoroutine (SpawnStuff ());
	}
	
	IEnumerator SpawnStuff (){
		while (true){
			yield return new WaitForSeconds(delay);
			Instantiate (obstacle, transform.position + Vector3.up * (Random.value * 4 - 2), Quaternion.identity);
		}
	}
}

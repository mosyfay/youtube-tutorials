﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

	Rigidbody2D myRigidBody;
	[SerializeField] float upSpeed;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey (KeyCode.Space)){
			myRigidBody.AddForce (Vector2.up * upSpeed);
		}
	}
}

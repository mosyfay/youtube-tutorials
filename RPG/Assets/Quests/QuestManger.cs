﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class QuestManger : MonoBehaviour {

	private static QuestManger instance;

	[SerializeField] private List<Quest> activeQuests = new List<Quest>();
	private List<Quest> completedQuests = new List<Quest>();

	void Awake (){
		if(instance){
			Debug.LogError ("You can only have one Quest Manager", this);
		}
		instance = this;
		DontDestroyOnLoad (gameObject);
	}

	public static void RaiseEvent (QuestEvent questEvent){
		foreach (Quest quest in instance.activeQuests){
			quest.PassEvent (questEvent);
			if(quest.isFailed) Debug.Log ("Quest failed");
			if(quest.isComplete) Debug.Log ("Quest Complete");
		}
	}
}


